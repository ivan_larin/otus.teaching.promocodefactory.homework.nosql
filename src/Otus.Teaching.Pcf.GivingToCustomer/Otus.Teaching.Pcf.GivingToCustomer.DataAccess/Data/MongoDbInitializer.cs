﻿using MongoDB.Driver;
using Otus.Teaching.Pcf.GivingToCustomer.Core.Domain;
using System;

namespace Otus.Teaching.Pcf.GivingToCustomer.DataAccess.Data
{
    public class MongoDbInitializer
        : IDbInitializer
    {
        private readonly IMongoDatabase _database;

        public MongoDbInitializer(IMongoDatabase database)
        {
            _database = database;
        }
        
        public void InitializeDb()
        {
            DropCollections();
            InsertData();
        }

        private void InsertData()
        {
            _database.GetCollection<Preference>(nameof(Preference)).InsertMany(FakeDataFactory.Preferences);
            _database.GetCollection<Customer>(nameof(Customer)).InsertMany(FakeDataFactory.Customers);
            _database.GetCollection<CustomerPreference>(nameof(CustomerPreference)).InsertMany(FakeDataFactory.CustomerPreferences);
        }

        private void DropCollections()
        {
            using (var collCursor = _database.ListCollections())
            {
                var colls = collCursor.ToList();
                foreach (MongoDB.Bson.BsonDocument col in colls)
                {
                    _database.DropCollection(col["name"].AsString);
                }
            }
        }
    }
}